import streamlit as st 
import pandas as pd
import numpy as np
import requests
import time

from pages import home, docs



st.write("# Welcome to Data specialist salary prediction!")

st.markdown('You can upload a dataset or entry data manually')


# if select load file then see requirement to csv file

st.sidebar.title('Navigation')

page = st.sidebar.selectbox('Select page', ['Home', 'Predictions', 'Docs', 'Contact'])

if page == 'Home':
    home.app()


  
if page == 'Predictions':
    st.markdown('## Predictions')
    selection = st.radio('Select data for prediction', ['Upload a dataset file', 'Entry data manually'])
    if selection == 'Upload a dataset file':
        # Drug and Drop file
        st.markdown('## Upload a dataset file' ) # you can use st.header too
        file_upload = st.file_uploader(label = 'Upload_csv')
        if file_upload is not None:
            st.write(pd.read_csv(file_upload)) 
    
        process_file = st.button(label="Process_file")

        if process_file:
            result = requests.post('https://predictor-xhtha3u7ba-lz.a.run.app/predict', json = pd.read_csv(file_upload).to_dict(orient='dict'))
            result_csv = pd.DataFrame(result.json()).to_csv(index=False)
        
            st.download_button(label='Download_csv', data=result_csv, file_name='predict.csv')

    if selection == 'Entry data manually':
        st.markdown('## Entry data manually')
        st.markdown('Predicted salary = ')
        st.sidebar.title('Entry data')

        # load unique data from df for each column
        job_title = st.sidebar.selectbox('Select job title', pd.read_csv('data/job_title.csv'))

        if job_title:
            st.write(job_title)

        job_title = st.selectbox('Select country', pd.read_csv('data/country_name.csv'))

        if job_title:
            st.write(job_title)