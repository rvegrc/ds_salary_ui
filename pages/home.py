import streamlit as st

def title():

    st.markdown('## Home')
    st.markdown('Introduction')
    st.markdown('### Introduction')
    st.markdown('### Data')
    st.markdown('### Features impotencies')


    st.markdown(
        '''
        This is a data specialist salary prediction
        '''
        )
def manual():
    st.markdown('''

    ## Instruction for data entry manually            

    **Experience_level**
                
    The experience level in the job during the year
    `EN` > Entry-level / Junior
    `MI`> Mid-level / Intermediate
    `SE` > Senior-level / Expert
    `EX` > Executive-level / Director

    **Employment_type**
                
    The type of employment for the role.

    `PT` > Part-time
    `FT` > Full-time
    `CT` > Contract
    `FL` > Freelance
                
    **Job_title** 
                
    The role worked in during the year.

    **Salary_currency** 
                
    The currency of the salary paid as an ISO 4217 currency code.

    USD: The salary in USD.

    **Employee_residence** 
                
    Employee's primary country of residence during the work year as an ISO 3166 country code.

    **Remote_ratio**
                
    The overall amount of work done remotely.

    **Company_location**
                
    The country of the employer's main office or contracting branch.

    **Company_size** 
                
    The median number of people that worked for the company during the year. 
                ''') 
    
def app():
    title()
    manual()
