import shutil
import os

# Check if the source file exists
def copy_file(src, dst):
    '''Copy the file from src to dst'''
    if os.path.isfile(src):
        try:
            shutil.copy(src, dst)
            print('File copied successfully.')
        except shutil.SameFileError:
            print('Source and destination represents the same file.')
        except PermissionError:
            print('Permission denied.')
        except Exception as e:
            print(f'Error occurred while copying file. {e}')
    else:
        print('Source file does not exist.')

    
src_1 = '../ds_salary/data/country_name.csv'

dst_1 = 'data/country_name.csv'

src_2 = '../ds_salary/data/job_title.csv'

dst_2 = 'data/job_title.csv'

# Call the copy_file function to copy src_1 to dst_1
copy_file(src_1, dst_1)

# Call the copy_file function to copy src_2 to dst_2
copy_file(src_2, dst_2)



